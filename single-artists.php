<?php get_header(); ?>
	<!-- Begin of loop -->

<?php if(have_posts()): ?>

	<?php while(have_posts()): the_post(); ?>
	
			<section id="intro" class="main style1 dark fullscreen">
				<div class="content container 75%">
					<header>
						<h2>Meet...</h2>
					</header>

					<?php $foto = get_field('foto'); ?>
					<div class="image-box"> <img src="<?php echo $foto['sizes']['thumbnail-mini']; ?>" alt="">  </div>

					<div class="author-name"> <h3><?php the_title(); ?></h3> </div>

							<footer>
								<a href="#one" class="button style2 down">More</a>
							</footer>
				</div>
			</section>	


			<?php $backg1 = get_field('background_alfa');?>
			<section id="one" class="main style2 right dark fullscreen" style='background-image:url(<?php echo $backg1;?>);'>
			
				<div class="content box style2 " >

					<header>
						<h2>What I Do</h2>
					</header>
					<?php $whatido = get_field('what_i_do');?>
					<p> <?php echo $whatido; ?></p>
				</div>
				<a href="#two" class="button style2 down anchored">Next</a>
			</section>


			<?php $backg2 = get_field('background_beta');?>
			<section id="two" class="main style2 left dark fullscreen" style='background-image:url(<?php echo $backg2;?>);'>
				<div class="content box style2">
					<header>
						<h2>Who I Am</h2>
					</header>
					<?php $whoiam = get_field('who_i_am') ?>
					<p> <?php echo $whoiam ?></p>
				</div>
				<a href="#work" class="button style2 down anchored">Next</a>
			</section>

			<section id="work" class="main style3 primary">
				<div class="content container">
					<header>
						<h2>My Work</h2>
					</header>

					<!-- Lightbox Gallery  -->
						<div class="container 75% gallery">

<!-- buscamos los comics --><?php  
 								$comiks = get_posts(array(
								'post_type' => 'comics',
								'meta_query' => array(
									array(
										'key' => 'author',
										'value' => '"' . get_the_ID() . '"',
										'compare' => 'LIKE',
									)
									)

								));
							?>

							<?php if($comiks):?>

								<?php foreach($comiks as $comics): ?>
									<?php $photo = get_field('portada', $comics-> ID); ?>
									<?php $status = get_field('status', $comics-> ID); ?>

							<div class="row 0% images">
								<div class="4u 12u(mobile)"><a href="<?php echo get_permalink($comics->ID); ?>"><img src="<?php echo $photo["sizes"]['thumbnail-mini'] ;?>" title="" alt="" /></a></div>
								<h4><?php echo get_the_title( $comics-> ID); ?></h4></br>
								
								<h6><?php echo $status[0]; ?></h6></br>
								<h5><?php print_r( get_field('status', $comics->ID)[0]); ?></h5>

							</div>

								<?php endforeach; ?>
							<?php endif; ?>


<!-- 							<div class="row 0% images">
								<div class="6u 12u(mobile)"><a href="images/fulls/03.jpg" class="image fit from-left"><img src="images/thumbs/03.jpg" title="Air Lounge" alt="" /></a></div>
								<div class="6u 12u(mobile)"><a href="images/fulls/04.jpg" class="image fit from-right"><img src="images/thumbs/04.jpg" title="Carry on" alt="" /></a></div>
							</div>
							<div class="row 0% images">
								<div class="6u 12u(mobile)"><a href="images/fulls/05.jpg" class="image fit from-left"><img src="images/thumbs/05.jpg" title="The sparkling shell" alt="" /></a></div>
								<div class="6u 12u(mobile)"><a href="images/fulls/06.jpg" class="image fit from-right"><img src="images/thumbs/06.jpg" title="Bent IX" alt="" /></a></div>
							</div> -->
						</div>

				</div>
			</section>


			<?php endwhile; ?>

		<?php endif; ?> 

<?php wp_reset_postdata(); ?>	
​
<!-- End of loop -->

<!-- new hope -->





<?php get_footer(); ?>