<?php get_header(); ?>

<!-- Begin of loop -->

<?php if(have_posts()): ?>

	<?php while(have_posts()): the_post(); ?>

<article>
​		<h2><a href="<?php the_permalink( ); ?>"><?php the_title( ); ?> </a></h2>

		<time><?php the_date( ); ?></time>
		<h6><?php the_author( ); ?></h6>
		<p><?php the_excerpt(); ?></p>
</article>

	<?php endwhile; ?>

<?php endif; ?> 

<?php wp_reset_postdata(); ?>	

<!-- End of loop -->

<?php get_footer(); ?>