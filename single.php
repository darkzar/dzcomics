<?php get_header(); ?>
	<!-- Begin of loop -->

<?php if(have_posts()): ?>

	<?php while(have_posts()): the_post(); ?>
		<?php $photo = get_field('portada'); ?>

	<section id="work" class="main style3 primary">
		<div class="content container">
			<header>
				<h2><?php the_title(); ?></h2>
				<p><?php the_date("Y-m-d"); ?></p>
				<?php the_permalink(); ?>
				<img src="<?php echo $photo["sizes"]['article-image'] ;?>" alt="">
			</header>

			<!-- Lightbox Gallery  -->

				<?php $pages = get_field('paginas'); ?>

				<div class="container 75% gallery">
					<div class="row 0% images">
						<div class="6u 12u(mobile)"><a href="<?php echo $pages; ?>" class="image fit from-left"><img src="<?php echo $pages[url]; ?>" title="" alt="" /></a></div>

					</div>

				</div>

		</div>
	</section>

	<?php endwhile; ?>

<?php endif; ?> 

<?php wp_reset_postdata(); ?>	

<!-- End of loop -->

<?php get_footer(); ?>