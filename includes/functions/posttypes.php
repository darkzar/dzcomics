<?php add_action( 'init', 'athelas_register_comic_autores' );
function athelas_register_comic_autores() {
    $labels = array(
        "name" => "Nombres de Artistas",
        "singular_name" => "Nombre de Artista",
        "menu_name" => "Artistas",
        "all_items" => "Lista de artistas", // aparece en sidebar
        "add_new" => "Agregar nuevo", //aparece en top y en sidebar
        "add_new_item" => "Agregar artista", // aparece en sidebar
        "edit" => "Editar",
        "edit_item" => "Editar info de artista",
        "new_item" => "Nuevo artista", //aparece en el interios
        "view" => "view",
        "view_item" => "Ver artista", //aparece en el interior - arriba derecha
        "search_items" => "Buscar artista",
        "not_found" => "Not found",
        "not_found_in_trash" => "Not found in the trash",
        );
    $args = array(
        "labels" => $labels,
        "description" => "Agrega info de artistas",
        'taxonomies' => array('tipos_plan'),
        "public" => true,
        "show_ui" => true,
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "artists", "with_front" => true ),
        "query_var" => true,
        "supports" => array( 'title', 'editor','post_tags' ),
        );
    register_post_type( "artists", $args );
}
    ?>

<?php add_action( 'init', 'athelas_register_comic_books' );
function athelas_register_comic_books() {
    $labels = array(
        "name" => "Comics",
        "singular_name" => "Comic",
        "menu_name" => "Comics",
        "all_items" => "Lista de comics", // aparece en sidebar
        "add_new" => "Agregar nuevo", //aparece en top y en sidebar
        "add_new_item" => "Agregar comic", // aparece en sidebar
        "edit" => "Editar",
        "edit_item" => "Editar info de comic",
        "new_item" => "Nuevo comic", //aparece en el interios
        "view" => "view",
        "view_item" => "Ver comic",
        "search_items" => "Buscar comic",
        "not_found" => "Not found",
        "not_found_in_trash" => "Not found in the trash",
        );
    $args = array(
        "labels" => $labels,
        "description" => "Agrega info de artistas",
        'taxonomies' => array('tipos_plan'),
        "public" => true,
        "show_ui" => true,
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "comics", "with_front" => true ),
        "query_var" => true,
        "supports" => array( 'title', 'editor','post_tags' ),
        );
    register_post_type( "comics", $args );
}
    ?>    