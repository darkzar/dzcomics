<?php 
// featured image activation and then sizing
add_action( 'init', 'athelas_image_support' );
function athelas_image_support(){
	add_theme_support( 'post-thumbnails' );
	// Just add an image name, the width, the height, and a boolean that defines if WP should crop the images or not
	// add as many as the project might require
	// smaller images do not get resized, but largeones will get cropped.
	//                name             width height crop?
	add_image_size( 'article-image', 800, 400, true);
	add_image_size( 'slider-image', 1200, 300, true);
	add_image_size( 'thumbnail-mini', 200, 200, false);
}
	
 ?>