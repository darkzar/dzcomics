<!DOCTYPE HTML>

<html>
	<head>
		<title><?php bloginfo('name');    wp_title(' | ', true, 'left');  	 ?> </title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link rel="stylesheet" href="assets/css/main.css" />
		<!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
	
	<?php wp_head(); ?>

	</head>

		<!-- Header -->
			<header id="header">

				<!-- Logo -->
					<h1 id="logo">Big Picture</h1>

				<!-- Nav -->

				<?php get_template_part("includes/templates/menues/menu", "main" ); ?>
			</header>
	

	<body <?php body_class(); ?> >