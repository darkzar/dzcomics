<?php get_header(); ?>

<!-- Begin of loop -->

<?php if(have_posts()): ?>

	<?php while(have_posts()): the_post(); ?>

<article>
​		<h2><a href="<?php the_permalink( ); ?>"><?php the_title( ); ?> </a></h2>

		<h6><?php the_permalink(); ?></h6>

		
<?php $image = get_field('portada'); ?>

		<img src="<?php echo $image["sizes"]['article-image']; ?> " alt="<?php echo $image["alt"]; ?>">
		<img src="<?php echo $image["sizes"]['slider-image']; ?> " alt="<?php echo $image["alt"]; ?>">
		<img src="<?php echo $image["sizes"]['thumbnail-mini']; ?> " alt="<?php echo $image["alt"]; ?>">

<?php $images = get_field('paginas'); ?>
	<?php foreach ($images as $imagex ): ?>
		<img src="<?php echo $imagex["sizes"]['medium_large']; ?> " alt="">


	<?php endforeach ?>
		<img src="<?php echo $images[1]["sizes"]['medium_large']; ?> " alt="">

</article>

	<?php endwhile; ?>

<?php endif; ?> 

<?php wp_reset_postdata(); ?>	

<!-- End of loop -->

<?php get_footer(); ?>