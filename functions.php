<?php 
	function enqueue_styles_and_scripts(){

	wp_enqueue_style('base', get_stylesheet_directory_uri().'/style.css' );
	wp_enqueue_style('font-awesome', get_stylesheet_directory_uri().'/css/font-awesome.min.css');	
	wp_enqueue_style('ie8', get_stylesheet_directory_uri().'/css/ie8.css');	
	wp_enqueue_style('ie9', get_stylesheet_directory_uri().'/css/ie9.css');			

	/******* SCRIPTS *******/

	wp_enqueue_script('jquery', get_template_directory_uri().'/js/jquery.min.js', null, null, true );
	wp_enqueue_script('poptrox', get_template_directory_uri().'/js/jquery.poptrox.min.js', array("jquery"), null, true );
	wp_enqueue_script('scrollex', get_template_directory_uri().'/js/jquery.scrollex.min.js', array("jquery"), null, true );
	wp_enqueue_script('main', get_template_directory_uri().'/js/jquery.main.min.js', array("jquery"), null, true );
	wp_enqueue_script('skel', get_template_directory_uri().'/js/jquery.skel.min.js', array("jquery"), null, true );			
	wp_enqueue_script('util', get_template_directory_uri().'/js/jquery.util.min.js', array("jquery"), null, true );	
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts');

	// registrando posttype artistas

	include('includes/functions/posttypes.php');
	include('includes/functions/image-sizes.php');	
	include('includes/functions/athelas-specials.php');		